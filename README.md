Run Jenkins locally for testing purpose
=======================================

This repository allows to run Jenkins locally on your machine for testing purpose.

It comes with 2 docker images:

- ics-jenkins with some default plugins (based on jenkins:2.46.3)
- ics-jenkins-slave with oracle-jdk 8 and maven 3.5.0 (based on centos:7) 

Requirements
------------

- docker
- docker-compose

Usage
-----

```
$ git clone https://bitbucket.org/europeanspallationsource/ics-docker-jenkins-test jenkins-test
$ cd jenkins-test
$ docker-compose build
$ docker-compose up -d
```

Jenkins will take some time to start, especially the first time.
You can check the logs with:

```
$ docker logs -f jenkinstest_jenkins_1
```

Open http://localhost:8080 in your browser and login as jenkins (password: jenkins).

License
-------

BSD 2-clause
